﻿using System;
using UEGP3.Core;
using UnityEngine;

namespace UEGP3.InventorySystem
{
	[RequireComponent(typeof(SphereCollider))]
	public class ItemPickup : MonoBehaviour, ICollectible
	{
		[Tooltip("The scriptable object of the item to be picked up.")] [SerializeField]
		public Item _itemToPickup;
		[Tooltip("Range in which the pick up is performed.")] [SerializeField] 
		private float _pickupRange;
		
		private SphereCollider _pickupCollider;

		// TODO 
		// while this is completely legit, I recommend moving this member to Item.cs 
		// If it is part of a scriptable object, we can take advantage of SO's properties: We only need to set the mesh once.
		// Otherwise, if I change the Item SO in the pickup, but not the mesh, I will experience a bug
		// Also vice versa of course. This is something small, but it is prone to errors! (Still full points though!)
        [SerializeField] private Mesh _newItemMesh;

        private void Awake()
		{
            // change the mesh of the item
            GetComponentInChildren<MeshFilter>().mesh = _newItemMesh;

            // Get collider on same object
            _pickupCollider = GetComponent<SphereCollider>();
			
			// Ensure collider values are set accordingly
			_pickupCollider.radius = _pickupRange;
			_pickupCollider.isTrigger = true;
		}

		public void Collect(Inventory inventory)
		{
			// Add item to inventory
			bool wasPickedUp = inventory.TryAddItem(_itemToPickup);

			// Destroy the pickup once the object has been successfully picked up
			if (wasPickedUp)
			{
				Destroy(gameObject);
			}
		}
		
#if UNITY_EDITOR
		private void OnValidate()
		{
			if (!_pickupCollider)
			{
				_pickupCollider = GetComponent<SphereCollider>();
			}
			
			// Ensure radius is set correctly
			_pickupCollider.radius = _pickupRange;
		}
#endif
	}
}