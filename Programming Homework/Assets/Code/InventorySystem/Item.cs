﻿using UnityEngine;

namespace UEGP3.InventorySystem
{
	public abstract class Item : ScriptableObject
	{
        public SpecifyItems SpecifyItems;
		[Tooltip("The name of the item")] [SerializeField]
		public string ItemName;
		[Tooltip("Short description of the item, shown to the player")] [SerializeField]
		public string Description;
		[Tooltip("A small icon of the item")] [SerializeField]
		public Sprite ItemSprite;
		[Tooltip("Is the item being consumed after usage?")] [SerializeField]
		public bool ConsumeUponUse;
		[Tooltip("A unique item can not be stacked in the players inventory")] [SerializeField]
		public bool IsUnique;
		
		/* // C# Auto-Property
		public bool ConsumeUponuse { get; set; }
		// C# Property: We can define more logic in get & set
		public Sprite ItemSprite {
			get
			{
				if (_isUnique)
				{
					return _uniqueSprite;
				}
				else
				{
					return _normalSprite;
				}
			}
			set
			{
				if (_consumeUponUse)
				{
					// Do some more logic
				}
				_itemSprite = value;
			}
		}
		*/

		/// <summary>
		/// Uses the item and executes its effect.
		/// </summary>
		public abstract void UseItem();
	}
}