﻿using System.Collections;
using System.Collections.Generic;
using UEGP3.InventorySystem;
using UnityEngine;

// the enum for the item specification
public enum SpecifyItems {Potion,Weapon, Key}

[CreateAssetMenu(menuName = "UEGP3/Inventory System/New Bag", fileName = "New Bag")]
public class Bag : ScriptableObject
{
    // TODO 
    // I think i saw this multiple times now in your code: Whenever you need a variable publicly, you just make it public.
    // Generally, when coding, we try to restrict access to our variables as much as possible. In the case of this MaxSize variable
    // you only need read-access outside of this script. If this is the case, we should also only expose this. The solution for that
    // is to make it a serializeField private float and add a public getter. This adds a bit code, but helps with cleaner API.
    // This way, the author of the code can decide whether or not a variable should be used publicly or not. Also this is a way of protecting
    // our code at least a little bit. 
    [Tooltip("Maximum amount of items that can be stored in the inventory")]
    [SerializeField]
    public int MaximumSize = 5;

    [Tooltip("categorize the Bag ")]
    public SpecifyItems specifyItems;


}
