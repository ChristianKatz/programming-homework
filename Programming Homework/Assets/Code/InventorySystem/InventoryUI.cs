﻿using UEGP3.InventorySystem;
using UnityEngine;

namespace UEGP3.InventorySystem
{
    public class InventoryUI : MonoBehaviour
    {

        [Tooltip("the parent of the inventory slots")]
        public Transform SlotsParent;

        // all Slots in the Scene
        InventorySlot[] _slots;

        [Tooltip("the scriptable object of the Inventory")]
        public Inventory PlayerInventory;

        [Header("Inventory Display")]
        [Tooltip("the parent of the potion bag display to deactivate & activate the bag UI")]
        [SerializeField] GameObject _potionBagDisplay;
        [Tooltip("the parent of the weapon bag display to deactivate & activate the bag UI")]
        [SerializeField] GameObject _weaponBagDisplay;
        [Tooltip("the parent of the key bag display to deactivate & activate the bag UI")]
        [SerializeField] GameObject _keyBagDisplay;
        [Header("Inventory Slots")]
        [Tooltip("the parent of the potion slots to deactivate & activate the slots")]
        [SerializeField] GameObject _potionBagSlots;
        [Tooltip("the parent of the weapon slots to deactivate & activate the slots")]
        [SerializeField] GameObject _weaponBagSlots;
        [Tooltip("the parent of the key slots to deactivate & activate the slots")]
        [SerializeField] GameObject _keyBagSlots;

        [Header("Inventory Menu Buttons")]
        [Tooltip("button that gets the player in the bag menu")]
        [SerializeField] private GameObject _bagsButton;
        [Tooltip("button that gets the player in the slot menu")]
        [SerializeField] private GameObject _slotsButton;
        [Tooltip("contains the parent of the bags to activate and deactivate them")]
        [SerializeField] private GameObject _bags;
        [Tooltip("the button that close all slots of the bags")]
        [SerializeField] GameObject _closeBagsButton;


        private void Start()
        {
            // get all inventory slots in the Scene
            _slots = SlotsParent.GetComponentsInChildren<InventorySlot>();
        }

        // update the UI if a item is collected
        // if the item is already in the inventory and not unique, stack it up
        // if the item is not in the inventory, add item
        public void UpdateUI(Item item)
        {
            for (int i = 0; i < _slots.Length; i++)
            {
                if (item == _slots[i].Item && !item.IsUnique)
                {
                    _slots[i].StackItem();
                    return;
                }

                if (i < PlayerInventory._inventoryItems.Count && _slots[i].Item == null)
                {
                    _slots[i].AddItem(item);
                }
            }
        }

        // open the Bags, when you press the Bag Button
        // deactivate the Inventory slots
        public void OpenBags()
        {
            SlotsParent.gameObject.SetActive(false);
            _slotsButton.SetActive(true);
            _bagsButton.SetActive(false);
            _bags.SetActive(true);
        }

        // close the Bags, when you press the Slot Button
        // activate the Inventory slots
        public void CloseBags()
        {
            SlotsParent.gameObject.SetActive(true);
            _slotsButton.SetActive(false);
            _bagsButton.SetActive(true);
            _bags.SetActive(false);
        }

        // open the Potion Bag
        // deactivate the other bags
        public void OpenPotionBag()
        {
            _closeBagsButton.SetActive(false);
            _potionBagSlots.SetActive(true);
            _potionBagDisplay.SetActive(false);
            _keyBagDisplay.SetActive(false);
            _weaponBagDisplay.SetActive(false);
        }

        // open the Weapon Bag
        // deactivate the other bags
        public void OpenWeaponBag()
        {
            _closeBagsButton.SetActive(false);
            _weaponBagSlots.SetActive(true);
            _potionBagDisplay.SetActive(false);
            _keyBagDisplay.SetActive(false);
            _weaponBagDisplay.SetActive(false);

        }

        // open the Key Bag
        // deactivate the other bags
        public void OpenKeyBag()
        {
            _closeBagsButton.SetActive(false);
            _keyBagSlots.SetActive(true);
            _potionBagDisplay.SetActive(false);
            _keyBagDisplay.SetActive(false);
            _weaponBagDisplay.SetActive(false);
        }

        // close all Slots, when the player wants to go back to the bag overview
        // activate all Bag Displays
        public void CloseBag()
        {
            _closeBagsButton.SetActive(true);
            _potionBagSlots.SetActive(false);
            _weaponBagSlots.SetActive(false);
            _keyBagSlots.SetActive(false);
            _potionBagDisplay.SetActive(true);
            _keyBagDisplay.SetActive(true);
            _weaponBagDisplay.SetActive(true);
        }
    }
}
