﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UEGP3.InventorySystem
{
    public class InventorySlot : MonoBehaviour
    {
        [Header("Slot Settings")]
        [Tooltip("The Icon of the Item ")]
        [SerializeField] private Image _icon;
        [Tooltip("The Name of the Item ")]
        [SerializeField] private TextMeshProUGUI _name;
        [Tooltip("The Description of the Item ")]
        [SerializeField] private TextMeshProUGUI _description;
        // TODO 
        // is there a reason this is not a serialized private field with a public getter to expose for usage?
        [Tooltip("The Numbers of the Item ")]
        public TextMeshProUGUI Numbers;

        // count the item amount
        public float ItemAmount = 0;

        // The Scriptable Object to have access to the items variables
        private Inventory _inventory;

        private void Start()
        {
            // TODO 
            // Doing it like this is okay, but we can optimize our code pretty easily here
            // Either we have a prfab of InventorySlot, where PlayerInventory is linked as a reference
            // or we get it from somewhere else (passed down from inventoryUI or whatnot). The reason
            // why we'd like to do this is, that FindObjectOfType is actually a quite expensive method invocation
            // and might cause lag spikes. Usually doing this in start/awake is okay, as it is only once, but for
            // this it might be problematic if we have e.g. large inventories. Nothing too bad, but keep in mind this is a thing.
            
            // get the Scriptable Object
            _inventory = FindObjectOfType<InventoryUI>().PlayerInventory;
        }

        // item that will be in the slot
        Item _item = null;
        public Item Item { get => _item; set => _item = value; }

        // add the item in the slot
        // set the description, name and icon of the item in the slot
        // activate the icon
        public void AddItem(Item newItem)
        {
            _item = newItem;

            _icon.sprite = newItem.ItemSprite;
            _icon.enabled = true;

            _name.text = newItem.ItemName;
            _description.text = newItem.Description;

            ItemAmount++;
            Numbers.text = ItemAmount.ToString();
        }

        // increase the item amount if the item is not unique and was collected twice or more
        public void StackItem()
        {
            ItemAmount++;
            Numbers.text = ItemAmount.ToString();
        }

        // clear the slot of the item information and the item itself
        public void ClearSlot()
        {
            _item = null;

            _icon.sprite = null;
            _icon.enabled = false;

            _description.text = null;
            _name.text = null;
            Numbers.text = null;
        }

        // if the button is clicked, the item amount will decrease 
        // if there are no items left, the slot will be cleared
        public void UseItem()
        {
            if (_item != null)
            {
                ItemAmount--;
                Numbers.text = ItemAmount.ToString();
                _inventory.UseItem(_item);

                if(ItemAmount <= 0)
                {
                    ClearSlot();
                }

            }
        }
    }
}


