﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace UEGP3.InventorySystem
{
    public class BagUI : MonoBehaviour
    {
        [Header("Bag Settings")]
        [Tooltip("the Scriptable Object of the bag")]
        public Bag Bag;
        [Tooltip("the Parent of the bag slots")]
        [SerializeField] GameObject _bagSlotsParent;
        [Tooltip("shows how many slots are used")]
        public TextMeshProUGUI SlotsUsed;

        // all bag slots of the slot parent
        BagSlot[] _bagSlots;

        // all Inventory slots in the Scene
        InventorySlot[] _slots;

        InventoryUI _inventoryUI;
        Inventory _inventory;

        // counts how many slots are used
        private float _counter;
        public float Counter { get => _counter; set => _counter = value; }

        private void Start()
        {
            // TODO
            // good job! why didnt you do it like this everywhere?! ;)
            
            // get the inventory of the player
            _inventory = FindObjectOfType<InventoryUI>().PlayerInventory;

            // get the inventory UI
            _inventoryUI = FindObjectOfType<InventoryUI>();

            // get the inventory slots of the inventory slots Parent
            _slots = _inventoryUI.SlotsParent.GetComponentsInChildren<InventorySlot>();

            // get the bag slots of the bag slots Parent
            _bagSlots = _bagSlotsParent.GetComponentsInChildren<BagSlot>();
        }

        // Add in the Player Inventory the corresponding items in the specify Bag
        // clear the item of the Inventory Dictionary and put it in the Bag Dictionary
        // decrease the numbers of items in the inventory and will be cleared if there are no items left
        // count the Item amount in the Bag and Update the UI numbers
        // Update the Inventory UI that the slot was cleared
        public void AddBagItems()
        {
            for (int i = 0; i < _slots.Length; i++)
            {
                if (_slots[i].Item != null)
                {
                    if (_slots[i].Item.SpecifyItems == Bag.specifyItems && _counter < Bag.MaximumSize)
                    {
                        _inventory.PutInBag(_slots[i].Item, Bag);

                        _slots[i].ItemAmount--;
                        if (_slots[i].ItemAmount <= 0)
                        {
                            _slots[i].ClearSlot();
                        }
                        _slots[i].Numbers.text = _slots[i].ItemAmount.ToString();

                        _counter++;
                        SlotsUsed.text = _counter.ToString();
                    }
                }
            }
        }

        // Updates the Bag UI if items will put in the Bag
        // if items are already in the Bag, they will stack up
        // if the item slot is not used, the item will be added
        public void UpdateBagUI(Item item)
        {
            for (int i = 0; i < _bagSlots.Length; i++)
            {
                if (item == _bagSlots[i].Item && !item.IsUnique)
                {
                    _bagSlots[i].StackItem();
                    return;
                }

                if (i < _inventory._inventoryBag.Count && _bagSlots[i].Item == null)
                {
                    _bagSlots[i].AddItem(item);
                    break;
                }

            }
        }
    }
}

