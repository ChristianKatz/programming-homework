﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UEGP3.InventorySystem
{
    // TODO
    // What you did makes sense! However I think we can "reduce" this whole slot logic to a single class, if bags use inventoryslots, which they technically could
    // This way, Items would also be added to the correct bag immediately. When I first tested your UI implementation without looking at the code, I didn't immediately
    // understand that I have to add them to bags manually. Your implementation is well thought-trhough and well executed. Of course there is room for improvements,
    // but thats always the case. If you want to get some insights on a different implementation approach, look at mine once you have the time for it!
    public class BagSlot : MonoBehaviour
    {
        [Header("Slot Settings")]
        [Tooltip("The Icon of the Item ")]
        [SerializeField] private Image _icon;
        [Tooltip("The Name of the Item ")]
        [SerializeField] private TextMeshProUGUI _name;
        [Tooltip("The Description of the Item ")]
        [SerializeField] private TextMeshProUGUI _description;
        [Tooltip("The Numbers of the Item ")]
        [SerializeField] private TextMeshProUGUI Numbers;

        // count the item amount
        private float _itemAmount = 0;
        public float ItemAmount { get => _itemAmount; set => _itemAmount = value; }

        // Scriptable Object to have access to the items variables
        private Inventory _inventory;

        private void Start()
        {
            // get the Scriptable Object
            _inventory = FindObjectOfType<InventoryUI>().PlayerInventory;
        }

        // item that will be in the slot
        Item _item = null;
        public Item Item { get => _item; set => _item = value; }

        // add the item in the slot
        // set the description, name and icon of the item in the slot
        // activate the icon
        public void AddItem(Item newItem)
        {
            _item = newItem;

            _icon.sprite = newItem.ItemSprite;
            _icon.enabled = true;

            _name.text = newItem.ItemName;
            _description.text = newItem.Description;

            _itemAmount++;
            Numbers.text = _itemAmount.ToString();
        }

        // increase the item amount if the item is not unique and was collected twice or more
        public void StackItem()
        {
            _itemAmount++;
            Numbers.text = _itemAmount.ToString();
        }

        // clear the slot of the item information and the item itself
        public void ClearSlot()
        {
            _item = null;

            _icon.sprite = null;
            _icon.enabled = false;

            _description.text = null;
            _name.text = null;
            Numbers.text = null;
        }

        // if the button is clicked, the item amount will decrease 
        // if there are no items left the slot will be cleared
        public void UseItem()
        {
            if (_item != null)
            {
                _itemAmount--;
                Numbers.text = _itemAmount.ToString();
                _inventory.UseItemInBag(_item);

                if (_itemAmount <= 0)
                {
                    ClearSlot();
                }

            }
        }
    }

}

