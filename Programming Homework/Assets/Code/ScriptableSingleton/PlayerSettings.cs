﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3.ScriptableSingleton
{
    // Scriptable Object that inherited of the ScriptableSingleton
    [CreateAssetMenu(fileName = "New PlayerSettings", menuName = "ScriptableSingleton/New PlayerSettings")]
    public class PlayerSettings : ScriptableSingleton<PlayerSettings>
    {
        public float PlayerSpeed;
        public float JumpForce;
        public float RotationSpeed;
    }
}


