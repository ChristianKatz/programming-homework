﻿using System.Linq;
using UnityEngine;

namespace UEGP3.ScriptableSingleton
{
    public abstract class ScriptableSingleton<T> : ScriptableObject where T : ScriptableObject
    {
        // the instance for the inherited class (Example: the ScriptableObject "PlayerSettings")
        static T _instance;

        // the property where the instance will be searched and loaded
        public static T Instance
        {
            // TODO 
            // Good job on figuring this out. The implementation is fine, but it uses Resources. 
            // Resources folder is okay to use, but should actually be avoided as much as possible.
            // Do you know why? 
            // (My implementation uses Resourses too though, because it was more convenient for this small 
			//  standalone task - in a real world project, I wouldn't be doing this though)! 
            get
            {
                if (!_instance)
                    _instance = Resources.LoadAll<T>("").FirstOrDefault();
                return _instance;
            }
        }
    }
}


