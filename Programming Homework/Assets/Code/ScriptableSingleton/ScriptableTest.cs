﻿using System.Collections;
using System.Collections.Generic;
using UEGP3.Core;
using UnityEngine;

namespace UEGP3.ScriptableSingleton
{
    // The test script for the ScriptableSingleton
    public class ScriptableTest : MonoBehaviour
    {
        void Update()
        {
            // press the K button to get the playerSpeed
            if (Input.GetKeyDown(KeyCode.K))
            {
                Debug.Log(PlayerSettings.Instance.PlayerSpeed);
            }
        }
    }
}

