﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3.PlayerSystem
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public class PlayerRigidbodyController : MonoBehaviour
    {
        [Header("Movement Settings")]
        [Tooltip("the speed which the character moves")]
        [SerializeField] private float _movementSpeed;
        [Tooltip("smooths the movement of the character")]
        [SerializeField] private float _smoothSpeed;
        [Tooltip("smooths the rotation of the character")]
        [SerializeField] private float _smoothRotation;
        [Tooltip("Physic Material of the character collider")]
        [SerializeField] private PhysicMaterial _physicMaterial;

        [Header("Jump Settings")]
        [Tooltip("the position of the sphere cast")]
        [SerializeField] private Transform _rayCastPosition;
        [Tooltip("the layer Mask, where the character is able to jump")]
        [SerializeField] private LayerMask _layerMask;
        [Tooltip("the radius, which the sphere should have")]
        [SerializeField] private float _castLenght;
        [Tooltip("Jump Power for the character")]
        [SerializeField] private float _jumpForce;
        private bool _isGrounded;

        // activates the jump in FixedUpdate
        private bool _jump = false;

        float _inputHorizontal;
        float _inputVertical;

        private Rigidbody _rigidbody;

        // the ref for the smooth movement
        private Vector3 _currentVelocity;

        // the direction, where the player wants to move
        Vector3 _desiredDirection;

        void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            // both inputs for our movement
            _inputHorizontal = Input.GetAxis("Horizontal");
            _inputVertical = Input.GetAxis("Vertical");

            PlayerInput();
            Jump();
            AntiSlide();
        }


        private void Jump()
        {
            RaycastHit hit;

            // fire the raycast down the y-axis to check if the character hit the ground
            // If the character is grounded and the spaceBar is pressed he is able to jump
            _isGrounded = Physics.Raycast(_rayCastPosition.position, -Vector3.up, out hit, _castLenght, _layerMask);

            if (_isGrounded)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    _jump = true;
                }
            }
        }

        // stop the sliding if the player stands still on a not horizontal surface
        private void AntiSlide()
        {
            // if the character gets input, the physic material is slippy to don't get stuck anywhere
            if (_desiredDirection.z > new Vector3(0, 0, 0).z || _desiredDirection.z < new Vector3(0, 0, 0).z && _desiredDirection.x > new Vector3(0, 0, 0).x || _desiredDirection.x < new Vector3(0, 0, 0).x)
            {
                _physicMaterial.frictionCombine = PhysicMaterialCombine.Minimum;
                _physicMaterial.dynamicFriction = 0;
                _physicMaterial.staticFriction = 0;
            }

            //if the character doesn't get any input, he should stand still
            if (_desiredDirection.z == new Vector3(0, 0, 0).z && _desiredDirection.x == new Vector3(0, 0, 0).x && _isGrounded)
            {
                _physicMaterial.dynamicFriction = 2;
                _physicMaterial.frictionCombine = PhysicMaterialCombine.Maximum;
            }
        }

        private void FixedUpdate()
        {
            // smooths the movement of the desired character direction
            // the player moves
            _rigidbody.velocity = Vector3.SmoothDamp(_rigidbody.velocity, _desiredDirection * _movementSpeed, ref _currentVelocity, _smoothSpeed);

            // if the jump value is true, the player jumps
            if (_jump == true)
            {
                _rigidbody.AddForce(new Vector3(0, _jumpForce, 0), ForceMode.Impulse);
                _jump = false;
            }

        }

        // if we have some input, the player rotate to the direction
        // if we have no input, the player shouldn't rotate back to his initial state and shouldn't move
        private void PlayerInput()
        {
            // gives us the input, when we press the button
            float vector3Length = new Vector3(_inputHorizontal, 0, _inputVertical).magnitude;

            // Clamp the vector length, so that we have the same speed regardless if are two keys pressed at the same time
            vector3Length = Mathf.Clamp(vector3Length, -1, 1);

            // if the player doesn't give input, then the player should stay in his rotation direction
            // if the player presses the buttons, then the player will rotate to his desired direction
            if (vector3Length >= 0.1f)
            {
                PlayerRotation();
            }

            // if we have no input the speed should be 0 to stop the character
            if (vector3Length <= 0)
            {
                _desiredDirection = new Vector3(0, 0, 0);
            }
        }

        // character rotates relative to the camera current vectors
        private void PlayerRotation()
        {
            // get the current vectors
            Vector3 forward = Camera.main.transform.forward;
            Vector3 right = Camera.main.transform.right;

            // we don't need the y values, because he shouldn't look upwards or downwards
            forward.y = 0;
            right.y = 0;

            // the direction where the player should look at when the player press the WASD keys
            _desiredDirection = forward * _inputVertical + right * _inputHorizontal;

            // the rotation where the player will rotate relative to the input
            Quaternion rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(_desiredDirection, Vector3.up), _smoothRotation);
            transform.rotation = rotation;
        }
    }

}

