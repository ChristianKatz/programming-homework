﻿using System;
using UnityEngine;

namespace UEGP3.PlayerSystem
{
	[RequireComponent(typeof(CharacterController))]
	public class PlayerController : MonoBehaviour
	{
		[Header("General Settings")] [Tooltip("The speed with which the player moves forward")] [SerializeField]
		private float _movementSpeed = 10f;
		[Tooltip("The graphical represenation of the character. It is used for things like rotation")] [SerializeField]
		private Transform _graphicsObject = null;
		[Tooltip("Reference to the game camera")] [SerializeField]
		private Transform _cameraTransform = null;

		[Header("Movement")] [Tooltip("Smoothing time for turns")] [SerializeField]
		private float _turnSmoothTime = 0.15f;
		[Tooltip("Smoothing time to reach target speed")] [SerializeField]
		private float _speedSmoothTime = 0.7f;
		[Tooltip("Modifier that manipulates the gravity set in Unitys Physics settings")] [SerializeField]
		private float _gravityModifier = 1.0f;
		[Tooltip("Maximum falling velocity the player can reach")] [Range(1f, 15f)] [SerializeField]
		private float _terminalVelocity = 10f;
		[Tooltip("The height in meters the cahracter can jump")] [SerializeField]
		private float _jumpHeight;

		// TODO
		// Minor correction: control not controll (dw this doesnt cost you any points^^)
        [Tooltip("At 1 the player has full controll about the character while jumping/falling")]
        [SerializeField] [Range(0, 1)] private float _airControll;
        private float _initialAircontroll;

        // the maximal target speed of the movement
        private float _targetSpeed;

        // _dashSpeedSmoothTime & _doublePressTime are not adjustable while runtime
        [Header("Dash")] 
        [Tooltip("The dash speed which the player moves abruptly forward")]
        [SerializeField] private float _dashSpeed = 500;
        [Tooltip("Smoothing time to reach target speed for the dash")]
        [SerializeField]private float _dashSpeedSmoothTime = 0.5f;
        [Tooltip("The time which the player has to press the W buttom twice to dash")]
        [SerializeField] float _doublePressTime = 0.2f;

        // TODO
        // A suggestions for such "timers": Create a variable _currentDoublePressTime instead. This way you never need to touch the original variable and
        // will still be able to manipulate it at runtime for testing iterations. You can then set _currentDoublePressTime = _doublePressTime when the timer starts
        // decrease the _currentTime and check if it is >= / <= 0. Another way of doing this is to compare the lastTime against the current time. 
        // For such implementations you usually have a _doublePressTimeWindow variable (serialized priavte field) and a _lastTimeButtonPressed variable. Whenever
        // the button gets pressed, you set _lastTimeButtonPressedn = Time.time. Wherever you need it, you can ask:
        // if (_lastTimeButtonPressed + _doublePressTimeWindow >= Time.time) do stuff. I encourage you to play around with the different kinds of implementations
        // to see what works best for your!
        
        // set back the doublePressTime to the original value
        private float _initialDoublePressTime;
        // activates the "_doublePressTime" to count down
        bool _dash = false;
        // says, if the E button was already pressed one time
        bool _alreadyPressed = false;
        // the initialSpeedSmoothTime to overide the _dashSpeedSmoothTime later
        float _initialSpeedSmoothTime;


        [Header("Ground Check")] [Tooltip("A transform used to detect the ground")] [SerializeField]
		private Transform _groundCheckTransform = null;
		[Tooltip("The radius around transform which is used to detect the ground")] [SerializeField]
		private float _groundCheckRadius = 0.1f;
		[Tooltip("A layermask used to exclude/include certain layers from the \"ground\"")] [SerializeField]
		private LayerMask _groundCheckLayerMask = default;

		private bool _isGrounded;
		private float _currentVerticalVelocity;
		private float _currentForwardVelocity;
		private float _speedSmoothVelocity;
		private CharacterController _characterController;
		
		private void Awake()
		{
            _initialDoublePressTime = _doublePressTime;
            _initialSpeedSmoothTime = _speedSmoothTime;

            _characterController = GetComponent<CharacterController>();
		}

		private void Update()
		{
			// Fetch inputs
			// GetAxisRaw : -1, +1 (0) 
			// GetAxis: [-1, +1]
			float horizontalInput = Input.GetAxisRaw("Horizontal");
			float verticalInput = Input.GetAxisRaw("Vertical");
			bool jumpDown = Input.GetButtonDown("Jump");

			// Calculate a direction from input data 
			Vector3 direction = new Vector3(horizontalInput, 0, verticalInput).normalized;
			
			// If the player has given any input, adjust the character rotation
			if (direction != Vector3.zero)
			{
				float lookRotationAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + _cameraTransform.eulerAngles.y;
				Quaternion targetRotation = Quaternion.Euler(0, lookRotationAngle, 0);
				_graphicsObject.rotation = Quaternion.Slerp(_graphicsObject.rotation, targetRotation, _turnSmoothTime);
			}

			// Calculate velocity based on gravity formula: delta-y = 1/2 * g * t^2
			// We ignore the 1/2 to safe multiplications and because it feels better.
			// Second Time.deltaTime is done in controller.Move()-call so we save one multiplication here.
			_currentVerticalVelocity += Physics.gravity.y * _gravityModifier * Time.deltaTime;
			
			// Clamp velocity to reach no more than our defined terminal velocity
			_currentVerticalVelocity = Mathf.Clamp(_currentVerticalVelocity, -_terminalVelocity, _jumpHeight);

            // override the dash smooth Speed if it was activated
            _speedSmoothTime = _initialSpeedSmoothTime;

            // the target speed for the WASD movement
            _targetSpeed = (_movementSpeed * direction.magnitude);
            
            // TODO
            // this can be simplified to if (Dash())
            
            // if the player presses the W Button twice in a short time intervall, the dash will be activated
            if(Dash() == true)
            {
                // ovveride the speedSmoothTime of the WASD movement
                _speedSmoothTime = _dashSpeedSmoothTime;

                // the target speed for the dash
                _targetSpeed = _dashSpeed;
            }

            // Calculate velocity vector based on gravity and speed
            // (0, 0, z) -> (0, y, z)           
			Vector3 velocity = _graphicsObject.forward * _currentForwardVelocity + Vector3.up * _currentVerticalVelocity;

            // Use the direction to move the character controller
            // direction.x * Time.deltaTime, direction.y * Time.deltaTime, ... -> resultingDirection.x * _movementSpeed
            // Time.deltaTime * _movementSpeed = res, res * direction.x, res * direction.y, ...
            _characterController.Move(velocity * Time.deltaTime);
			
			// Check if we are grounded, if so reset gravity
			_isGrounded = Physics.CheckSphere(_groundCheckTransform.position, _groundCheckRadius, _groundCheckLayerMask);
			if (_isGrounded)
			{
                _currentForwardVelocity = Mathf.SmoothDamp(_currentForwardVelocity, _targetSpeed, ref _speedSmoothVelocity, _speedSmoothTime);

                // Reset current vertical velocity
                _currentVerticalVelocity = 0f;
			}

			// TODO
			// Statements like these should generally be simplified to if (!aBool), as it is shorter and hence more readable. 
			// In this case, we could've even added an else branch instead of a new if-statement
			// Your air control implementation varies from what I had in mind. At the bottom of this script, I left a method that you can use and play around with.
			// My idea of air control was that the player can not change direction after he pressed jump. E.g. in Super Mario World, you can move Mario freely while jumping
			// In other games, e.g. GTA once you press jump, you're moving along a fixed path trajectory. Your implementation is technically a form of air control
			// so I will give you 75% for this specific task, instead of the full 100. 
            if(_isGrounded == false)
            {
                _currentForwardVelocity = Mathf.SmoothDamp(_currentForwardVelocity, _targetSpeed * _airControll, ref _speedSmoothVelocity, _speedSmoothTime);
            }

			// If we are grounded and jump was pressed, jump
			if (_isGrounded && jumpDown)
			{
                
                // Use formula: Mathf.Sqrt(h * (-2) * g)
                _currentVerticalVelocity = Mathf.Sqrt(_jumpHeight * -2 * Physics.gravity.y);
			}
		}

		// TODO 
		// I like this too! Actually, I think it would be great to assign a dash into a specific direction to the respective key.
		// A + A would result in a forward dash and D + D in a dash to the right. 
		
        // NOTE
        // Actually the Dash Method was for the W Button that the player has to press twice in a short time interval
        // Then I noticed that he had to dash in all direction, so I just took the E Button and keep the double press mechanic, because I kinda like it (although it is actually unnecessary)

        // TODO
        // I think this method could've used some more comments on the separate parts. A few if branches are not completely straight forward.
        // I'll mark them below!
        // Another note: I think this could've been easier to implement. It would be enough, to have something like this:
        // private float _lastTimeDashPressed;
        // [SerializeField] private float _doublePressTimeWindow = 2.0f;
        // private bool ShouldDash()
        // {
	       //  if (Input.GetKeyDown(KeyCode.E))
	       //  {
		      //   // second part of if only evaluates if _alreadyPressed == true
		      //   if (_alreadyPressed && EvaluateDoublePressTimeWindow())
		      //   {
			     //    return true;
		      //   }
        //
		      //   _alreadyPressed = true;
	       //  }
        //
	       //  return false;
	       //  bool EvaluateDoublePressTimeWindow()
	       //  {
		      //   if (_alreadyPressed && (_lastTimeDashPressed + _doublePressTimeWindow > Time.time))
		      //   {
			     //    return true;
		      //   }
        //
		      //   return false;
	       //  }
        // }
        
        // the character dash along the Z-Axis
        // the player has to press the E Button twice in a very short time interval to activate the dash
        private bool Dash()
        {
	        // TODO 
	        // Whenever we are working with Input, it is considered best practice to use Input.GetButton method variations. The background is
	        // that for GetButton, you need a button name. This name then maps to an assigned button. This seems like a trivial thing, but 
	        // when we want to remap this, it is actually way easier to change. Instead of in potentially many parts of the code, we just 
	        // need to change it in one place. Let alone button remapping done by players (in options menus)!
            if (Input.GetKeyDown(KeyCode.E) && _alreadyPressed == false)
            {
	            // TODO
	            // I would suggest renaming this variable. The name is not speaking for it self, reading this name without context
	            // leaves you in a wild guess what has been pressed. My suggestion would be something alike _receivedFirstDashInput 
	            // This describes that it was the first input and it is related to the dash. Anything that clarifies these two is a valid name i guess!
                _alreadyPressed = true;
                _dash = true;
                return false;
            }
            else if (Input.GetKeyDown(KeyCode.E) && _alreadyPressed == true)
            {
	            // TODO
	            // same as above, unlike you re-use the variable for different purposes it should contain the info that its related to the dash:
	            // _dashDoublePressTimeFrame
                if (_doublePressTime >= 0)
                {
                    _doublePressTime = _initialDoublePressTime;
                    // TODO
                    // this is especially unintuitive: The method is named Dash. In it, you control a member variable _dash that you set to false
                    // when the dash should actually start. I think in this case we can get rid off this completely, _alreadyPressed and _dash share
                    // the exact same behaviour/responsibility! 
                    _dash = false;
                    _alreadyPressed = false;
                    return true;
                }
            }

            // TODO
            // not too obvious what/why this happens
            // in fact i think I would make a separate method out of this two statements. 
            if(_doublePressTime >= 0 && _dash == true)
            {
                _doublePressTime -= Time.deltaTime;

            }
            // TODO
            // not too obvious what/why this happens
            if (_doublePressTime <= 0)
            {
                _doublePressTime = _initialDoublePressTime;
                _dash = false;
                _alreadyPressed = false;
                // TODO
                // duplicate return statement: there is no code that is blocked if we return here. Not that much of a problem
                // but a lot of return statements often tend to make simple logic complicated. I would remove it!
                return false;
            }
            
            // TODO
            // Personally I like to introduce a variable at the beginning of my method with a suitable name (in this case maybe allowDash)
            // and just set the variable in the code and then return it in the end. especially if the logic is long/complex. For short methods simple returns are fine!
            return false;
        }
    }
}
