﻿using UEGP3.Core;
using UEGP3.InventorySystem;
using UnityEngine;


namespace UEGP3.PlayerSystem
{
	/// <summary>
	/// Player containing additional logic like item pick ups etc.
	/// </summary>
	public class Player : MonoBehaviour
	{
		[Tooltip("Inventory to be used for the player")] 
		[SerializeField] Inventory _playerInventory;

        // the whole Inventory UI to deactivate and activate it
        [SerializeField] GameObject _inventory;

        // TODO 
        // I suggest naming this to something like _isInventoryOpen and inverting the functionality of it below
        // This is something very nit-picky, I know, but for me the name you chose implies, that the inventory is open
        // if it evaluates to true. If you want to keep it like this, I suggest modifying it to be something like _canOpenInventory
        // At least from my experience, or me personally, this makes things easier to understand!
        // determines if the menu can be opened or closed
        private bool _openInventory = true;

        private void Update()
		{
            // Show Inventory if button is pressed
            // Cursor is locked and invisible when the inventory is closed
            // Cursor in unlocked and visible when the inventory is open
            if (Input.GetKeyDown(KeyCode.I))
            {
                if (_openInventory)
                {
                    _inventory.SetActive(true);
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;
                    _openInventory = false;

                }
                else
                {
                    _inventory.SetActive(false);
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;
                    _openInventory = true;
                }
            }
		}

        private void OnTriggerEnter(Collider other)
		{
			// If we collide with a collectible item, collect it
			ICollectible collectible = other.gameObject.GetComponent<ICollectible>();
			if (collectible != null)
			{
                Collect(collectible);
			}
		}

		private void Collect(ICollectible collectible)
		{
			collectible.Collect(_playerInventory);
		}
	}
}